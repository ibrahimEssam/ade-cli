.. _install:

Installation
------------

Requirements
^^^^^^^^^^^^

* Docker 19.03 or newer, follow the `instructions for your OS`_
* If the host machine has an NVIDIA graphics card, see the :ref:`nvidia-docker` article


Installation
^^^^^^^^^^^^

Linux x86_64 and aarch64
========================

#. Verify that the Requirements above are fulfilled
#. Download the statically-linked binary from the `Releases`_ page of the ``ade-cli`` project
#. Name the binary ``ade`` and install it in your ``PATH``
#. Make the binary executable: ``chmod +x ade``
#. Check that it is installed:

.. code:: bash

   $ which ade
   /path/to/ade
   $ ade --version
   <version>


For information on how to install ADE (``ade-cli``, ADE base image, and ADE volumes)
on an offline machine, see :ref:`offline-install`.

.. toctree::
   :maxdepth: 4
   :hidden:

   offline-install


OSX (Experimental)
==================
#. Verify that the Requirements above are fulfilled
#. Clone the repository and run the ``osx-install`` script:

.. code:: bash

   $ git clone https://gitlab.com/ApexAI/ade-cli
   $ cd ade-cli
   $ ./osx-install
   $ which ade
   ~/.local/bin/ade
   $ ade --version
   <version>


Running X11 Apps on OSX
~~~~~~~~~~~~~~~~~~~~~~~

.. note::
   Running GUI apps on OSX is an experimental feature and requires some additional
   programs.

#. Install `Homebrew`_
#. Use Homebrew to install ``socat``:

   .. code:: bash

      $ brew install socat

#. Install XQuartz v2.7.11:

   .. code:: bash

      $ brew cask install xquartz

#. Restart your computer
#. Open XQuartz -> Preferences -> Security and check 'Allow connections from network clients'

   .. warning::
      The security implication of allowing connections from network clients has not been
      fully analyzed. Enable at your own risk.

#. Test UI apps: e.g. in an Ubuntu-based ADE image:

   .. code:: bash

      $ sudo apt-get update && sudo apt-get install -y x11-apps
      $ xeyes


Update
^^^^^^

To update ``ade-cli``, run ``ade update-cli``. If a newer version is available,
``ade`` will prompt for confirmation, download the new version, and replace itself.


Autocompletion
^^^^^^^^^^^^^^

To enable autocompletion, add the following to your ``.zshrc`` or ``.bashrc``:

   .. literalinclude:: ../completion.sh
      :language: shell
      :lines: 3-


See :ref:`usage` for next steps.

.. _Releases: https://gitlab.com/ApexAI/ade-cli/-/releases
.. _instructions for your OS: https://docs.docker.com/install/#server
.. _Homebrew: https://brew.sh/
